package ugolokSearch.stepsAddress;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static ugolokSearch.utils.Commons.baseUrl;

public class APISteps {
    private static Response response;
    private RequestSpecification request;

    @Given("I have existing address {}")
    public void accessToken(String q) {
        request = RestAssured.given()
                .header("Content-Type", "application/json")
                .params("accept-language", "ru",
                        "countrycodes", "ua",
                        "q", q)
                .expect().request();
    }

    @When("I make GET request for the endpoint")
    public void getRequestAddress() {
        response = request.get(baseUrl);
        response.prettyPrint();
    }

    @Then("I see status code {}")
    public void statusCode(Integer code) {
        Integer statusCode = response.getStatusCode();
        assertEquals(code, statusCode);
    }

    @And("Create address response should be valid {}")
    public void verifyResponse(String display_name) {
        response.then()
                .assertThat()
                .body("[0].display_name", equalTo(display_name));
    }
}