Feature: Add Address

  @api
  Scenario: Get address latin
    Given I have existing address gjpljd;yz dekbwz [fhmrbd [fhrbdcmrf j,kfcnm
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Поздовжня вулиця, Харків, Харківська область

  @api
  Scenario: Get address cyrillic
    Given I have existing address Поздовжня вулиця, Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Поздовжня вулиця, Харків, Харківська область
  @api
  Scenario: Get address centralStreets
    Given I have existing address Сумська вулиця, Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Сумська вулиця, Харків, Харківська область
  @api
  Scenario: Get address periphery
    Given I have existing address Залютинський в’їзд, Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Залютинський в’їзд, Харків, Харківська область
  @api
  Scenario: Get address largeCities
    Given I have existing address Людвіга Свободи проспект, Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Людвіга Свободи проспект, Харків, Харківська область
  @api
  Scenario: Get address villages
    Given I have existing address Вишнева вулиця, Буди, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Вишнева вулиця, Буди, Харківська область
  @api
  Scenario: Get address fullNames
    Given I have existing address Гацева вулиця, 1 Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Гацева вулиця, 1 Харків, Харківська область
  @api
  Scenario: Get address withHouseNumbers
    Given I have existing address Поздовжня вулиця, 3а Харків, Харківська область
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Поздовжня вулиця, 3а Харків, Харківська область
  @api
  Scenario: Get address partialStreetNames
    Given I have existing address Поздовж 3 хар
    When  I make GET request for the endpoint
    Then  I see status code 200
    And   Create address response should be valid Поздовжня вулиця, 3 Харків, Харківська область
